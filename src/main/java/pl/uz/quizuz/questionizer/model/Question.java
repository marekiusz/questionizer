package pl.uz.quizuz.questionizer.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Question {

    private UUID id;
    private int categoryId;
    private String question;
    private String correctAnswer;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;

    public Question(final int categoryId,
                    final String question,
                    final String correctAnswer,
                    final String answer1,
                    final String answer2,
                    final String answer3,
                    final String answer4) {
        this.categoryId = categoryId;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
    }
}

