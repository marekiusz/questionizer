package pl.uz.quizuz.questionizer.model;

import lombok.Getter;

public enum Category {

    INFORMATYKA(1),
    MUZYKA(2),
    MATEMATYKA(3),
    FILMY(4);

    @Getter
    private final int categoryId;

    Category(final int categoryId) {
        this.categoryId = categoryId;
    }
}
