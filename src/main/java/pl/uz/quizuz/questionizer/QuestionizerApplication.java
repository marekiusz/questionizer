package pl.uz.quizuz.questionizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionizerApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuestionizerApplication.class, args);
    }

}
