package pl.uz.quizuz.questionizer.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.uz.quizuz.questionizer.model.Category;
import pl.uz.quizuz.questionizer.service.QuestionsReadService;

@RestController
public class GetQuestionsByCategoryController {

    private final QuestionsReadService questionsReadService;

    @Autowired
    public GetQuestionsByCategoryController(final QuestionsReadService questionsReadService) {
        this.questionsReadService = questionsReadService;
    }

    @GetMapping("/questions/{category}")
    public String getQuestionsByCategory(@PathVariable final String category) {
        return questionsReadService.getQuestionsByCategory(category);
    }
}
