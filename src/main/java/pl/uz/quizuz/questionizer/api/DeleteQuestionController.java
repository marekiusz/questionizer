package pl.uz.quizuz.questionizer.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.uz.quizuz.questionizer.service.QuestionsService;

@RestController
public class DeleteQuestionController {

    private final QuestionsService questionsService;

    @Autowired
    public DeleteQuestionController(final QuestionsService questionsService) {
        this.questionsService = questionsService;
    }

    @DeleteMapping("/questions/delete/{questionId}")
    public String addQuestion(@PathVariable final String questionId) {
        return questionsService.deleteQuestion(questionId);
    }
}
