package pl.uz.quizuz.questionizer.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.uz.quizuz.questionizer.service.QuestionsReadService;

@RestController
public class GetQuestionsController {

    private final QuestionsReadService questionsReadService;

    @Autowired
    public GetQuestionsController(final QuestionsReadService questionsReadService) {
        this.questionsReadService = questionsReadService;
    }

    @GetMapping("/questions")
    public String getQuestions() {
        return questionsReadService.getQuestions();
    }
}
