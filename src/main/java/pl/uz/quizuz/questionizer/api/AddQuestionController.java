package pl.uz.quizuz.questionizer.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.uz.quizuz.questionizer.model.Question;
import pl.uz.quizuz.questionizer.service.QuestionsService;

@RestController
public class AddQuestionController {

    private final QuestionsService questionsService;

    @Autowired
    public AddQuestionController(final QuestionsService questionsService) {
        this.questionsService = questionsService;
    }

    @PostMapping("/questions/add")
    public String addQuestion(@RequestBody final Question question) {
        return questionsService.addQuestion(question);
    }
}
