package pl.uz.quizuz.questionizer.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.uz.quizuz.questionizer.service.CategoriesReadService;

@RestController
public class GetCategoriesController {

    private final CategoriesReadService categoriesReadService;

    @Autowired
    public GetCategoriesController(final CategoriesReadService categoriesReadService) {
        this.categoriesReadService = categoriesReadService;
    }

    @GetMapping("/categories")
    public String getCategories() {
        return categoriesReadService.getCategories();
    }
}
