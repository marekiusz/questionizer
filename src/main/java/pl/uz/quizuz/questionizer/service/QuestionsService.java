package pl.uz.quizuz.questionizer.service;

import lombok.extern.slf4j.Slf4j;
import net.thegreshams.firebase4j.error.FirebaseException;
import net.thegreshams.firebase4j.model.FirebaseResponse;
import net.thegreshams.firebase4j.service.Firebase;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.uz.quizuz.questionizer.model.Question;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
public class QuestionsService {

    private final Firebase firebase;

    @Autowired
    public QuestionsService(final Firebase firebase) {
        this.firebase = firebase;
    }

    public String addQuestion(final Question question) {
        final ObjectMapper objectMapper = new ObjectMapper();
        question.setId(UUID.randomUUID());
        try {
            final String questionJSON = objectMapper.writeValueAsString(question);
            final FirebaseResponse firebaseResponse = firebase.post("questions", questionJSON);
            return firebaseResponse.getRawBody();
        } catch (IOException | FirebaseException e) {
            log.warn("There was problem adding new question: {0}", e);
        }
        return null;
    }

    public String deleteQuestion(final String questionId) {
        try {
            final Firebase queryFirebase = new Firebase("https://quizuz.firebaseio.com");
            final FirebaseResponse firebaseResponse = queryFirebase
                    .addQuery("orderBy", "\"id\"")
                    .addQuery("equalTo", "\"" + questionId + "\"")
                    .get("questions");
            final Map<String, Object> body = firebaseResponse.getBody();
            final String questionFirebaseId = body.keySet().iterator().next();
            return firebase.delete("questions/" + questionFirebaseId).getRawBody();
        } catch (FirebaseException | UnsupportedEncodingException e) {
            log.warn("There was problem fetching questions: {0}", e);
        }
        return null;
    }
}
