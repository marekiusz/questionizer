package pl.uz.quizuz.questionizer.service;

import lombok.extern.slf4j.Slf4j;
import net.thegreshams.firebase4j.error.FirebaseException;
import net.thegreshams.firebase4j.model.FirebaseResponse;
import net.thegreshams.firebase4j.service.Firebase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.uz.quizuz.questionizer.model.Category;

import java.io.UnsupportedEncodingException;

@Slf4j
@Service
public class QuestionsReadService {

    private final Firebase firebase;

    @Autowired
    public QuestionsReadService(final Firebase firebase) {
        this.firebase = firebase;
    }

    public String getQuestions() {
        try {
            final FirebaseResponse firebaseResponse = firebase.get("questions");
            return firebaseResponse.getRawBody();
        } catch (FirebaseException | UnsupportedEncodingException e) {
            log.warn("There was problem fetching questions: {0}", e);
        }
        return null;
    }

    public String getQuestionsByCategory(final String category) {
        final Category selectedCategory = Enum.valueOf(Category.class, category);
        final String categoryId = String.valueOf(selectedCategory.getCategoryId());
        try {
            final Firebase queryFirebase = new Firebase("https://quizuz.firebaseio.com");
            final FirebaseResponse firebaseResponse = queryFirebase
                    .addQuery("orderBy", "\"categoryId\"")
                    .addQuery("equalTo", categoryId)
                    .get("questions");
            return firebaseResponse.getRawBody();
        } catch (FirebaseException | UnsupportedEncodingException e) {
            log.warn("There was problem fetching questions: {0}", e);
        }
        return null;
    }
}
