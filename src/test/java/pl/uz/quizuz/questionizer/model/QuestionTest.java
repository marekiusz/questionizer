package pl.uz.quizuz.questionizer.model;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class QuestionTest {

    private Question testQuestion;

    @Before
    public void setUp() {
        testQuestion = new Question(1,
                "Is that a test question?",
                "Yes",
                "Yes",
                "No",
                "Maybe",
                "Not really");
    }

    @Test
    public void shouldGetCategoryId() {
        // given
        final int categoryId = 1;

        // when
        final int result = testQuestion.getCategoryId();

        // then
        assertThat(result).isEqualTo(categoryId);
    }

    @Test
    public void shouldGetQuestion() {
        // given
        final String question = "Is that a test question?";

        // when
        final String result = testQuestion.getQuestion();

        // then
        assertThat(result).isEqualTo(question);
    }

    @Test
    public void shouldGetCorrectAnswer() {
        // given
        final String correctAnswer = "Yes";

        // when
        final String result = testQuestion.getCorrectAnswer();

        // then
        assertThat(result).isEqualTo(correctAnswer);
    }

    @Test
    public void shouldGetAnswer1() {
        // given
        final String answer = "Yes";

        // when
        final String result = testQuestion.getAnswer1();

        // then
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldGetAnswer2() {
        // given
        final String answer = "No";

        // when
        final String result = testQuestion.getAnswer2();

        // then
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldGetAnswer3() {
        // given
        final String answer = "Maybe";

        // when
        final String result = testQuestion.getAnswer3();

        // then
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldGetAnswer4() {
        // given
        final String answer = "Not really";

        // when
        final String result = testQuestion.getAnswer4();

        // then
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldSetCategoryId() {
        // given
        final int categoryId = 2;

        // when
        testQuestion.setCategoryId(categoryId);

        // then
        final int result = testQuestion.getCategoryId();
        assertThat(result).isEqualTo(categoryId);
    }

    @Test
    public void shouldSetQuestion() {
        // given
        final String question = "Really?";

        // when
        testQuestion.setQuestion(question);

        // then
        final String result = testQuestion.getQuestion();
        assertThat(result).isEqualTo(question);
    }

    @Test
    public void shouldSetCorrectAnswer() {
        // given
        final String correctAnswer = "Really?";

        // when
        testQuestion.setCorrectAnswer(correctAnswer);

        // then
        final String result = testQuestion.getCorrectAnswer();
        assertThat(result).isEqualTo(correctAnswer);
    }

    @Test
    public void shouldSetAnswer1() {
        // given
        final String answer = "Really?";

        // when
        testQuestion.setAnswer1(answer);

        // then
        final String result = testQuestion.getAnswer1();
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldSetAnswer2() {
        // given
        final String answer = "Really?";

        // when
        testQuestion.setAnswer2(answer);

        // then
        final String result = testQuestion.getAnswer2();
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldSetAnswer3() {
        // given
        final String answer = "Really?";

        // when
        testQuestion.setAnswer3(answer);

        // then
        final String result = testQuestion.getAnswer3();
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldSetAnswer4() {
        // given
        final String answer = "Really?";

        // when
        testQuestion.setAnswer4(answer);

        // then
        final String result = testQuestion.getAnswer4();
        assertThat(result).isEqualTo(answer);
    }

    @Test
    public void shouldReturnToString() {
        // given
        final String strQuestion = "Question(id=null, " +
                "categoryId=1, " +
                "question=Is that a test question?, " +
                "correctAnswer=Yes, " +
                "answer1=Yes, " +
                "answer2=No, " +
                "answer3=Maybe, " +
                "answer4=Not really)";

        // when
        final String result = testQuestion.toString();

        // then
        assertThat(result).isEqualTo(strQuestion);
    }
}