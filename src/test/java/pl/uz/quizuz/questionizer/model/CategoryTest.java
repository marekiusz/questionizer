package pl.uz.quizuz.questionizer.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class CategoryTest {

    @Test
    public void shouldGetCategoryId() {
        // given
        final Category category = Category.INFORMATYKA;
        final int categoryId = 1;

        // when
        final int result = category.getCategoryId();

        // then
        assertThat(result).isEqualTo(categoryId);
    }
}